/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Observer;

/**
 *
 * @author Ezequiel
 */
public interface ISujeto{
    void entregar();
    void addObserver(IObserver obs);
    void removeObserver(IObserver obs);
    
}
