/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Observer;

import java.util.ArrayList;

/**
 *
 * @author Ezequiel
 */

public class Periodico implements ISujeto {
  ArrayList <IObserver> Observers;  

    @Override
    public void entregar() {
        for(IObserver holis: Observers){
            holis.recibir();
        }
    
    }

    @Override
    public void addObserver(IObserver obs) {
        if(Observers==null){
            Observers=new ArrayList<IObserver>();
        }
        Observers.add(obs);
    }

    @Override
    public void removeObserver(IObserver obs) {
        if(Observers.contains(obs)!=false)
            Observers.remove(obs);
    }
}
